import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './containers/header/header';
import App_content from './containers/container/container';
import Footer from './containers/footer/footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <App_content/>
        <Footer/>
      </div>
    );
  }
}

export default App;
