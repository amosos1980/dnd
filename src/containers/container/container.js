import React, { Component } from 'react';
import './container.css';

import SideBar from './SideBar/side-bar';
import DraggZone from './DraggZone/dragg-zone';

class App_content extends Component {
  render() {
    return (
      <div className="Container">
        <SideBar/>
        <DraggZone/>
      </div>
    );
  }
}

export default App_content;
